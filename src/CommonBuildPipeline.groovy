

def runPipeline() {
    def apigeeBuilder = new ApigeeBuilder()  
  def tmpBuilddata = [:]
  def builddata = []
  def repoName = ""
  def buildVersion = ""
  def artifactoryServer
  def mvnName
  
  def branchName = "${env.BRANCH_NAME}".replace("/", "-").toLowerCase()
  if (!branchName) {
    branchName = "master"
  }
  

    node() {
      mvnName= tool 'maven3'
      withCredentials([usernamePassword(credentialsId: 'NEXUSID', passwordVariable: 'password', usernameVariable: 'username')]) {
       //artifactoryServer = Artifactory.newServer url: "http://localhost:1234/", username: username, password: password
     }
      
      currentBuild.result = 'SUCCESS'
      try {
        stage("Git Pull") {
          
          def scmVars = checkout scm
          //config loading goes here
          repoName = scm.getUserRemoteConfigs()[0].getUrl().tokenize('/').last().split("\\.")[0]
          print repoName
          
          tmpBuilddata = readYaml file: "build.yaml"
          buildVersion = tmpBuilddata.version
           
          if (!buildVersion) {
              throw new Exception("Project version not found.")
          }


            dir("${workspace}") {
              tmpBuilddata["git_hash"] =  sh(returnStdout: true, script: "git rev-parse HEAD").trim()
              tmpBuilddata["git_shorthash"] = sh(returnStdout: true, script: "git rev-parse --short=6 HEAD").trim()
              tmpBuilddata["git_commit"] = sh (script: "git log -1| sed 1d ", returnStdout: true).trim()
            }


            // Set common variables for each build config
            println tmpBuilddata


             tmpBuilddata.apigee.each {
               it.workingdir = "${workspace}"
               it.proj = load "${it.workingdir}/DevOpsProject.groovy"
               it.branch = branchName
               it.jobname = "${env.JOB_NAME}".replace(" ", "-").toLowerCase()
               it.repoName = repoName
               it.buildtype = "apigee"
               it.pomDir = tmpBuilddata.apigee.pom_dir[0]
               it.version = "${buildVersion}_${tmpBuilddata["git_shorthash"]}"
               it.name = "${repoName}-${it.name}".replace(" ", "-").toLowerCase()
               it.arttifactserver = tmpBuilddata.deployment.artifact_server_url
               it.mvnName = mvnName;
               builddata.push(it)
             }
             
            println "Builddata: ${builddata}"
            if (tmpBuilddata.docker.size() > 0) {
                hasdocker = true
            }
        }

        

        // Pre build tasks
        stage ("Pre build tasks") {
          parallel apigeeBuilder.generateSteps("Pre Build", builddata) {
            switch (it.buildtype) {
        
              case "apigee":
               return apigeeBuilder.prebuild(it)
            }
          }
        }

        // Code Quaility Scan
        stage ("Code Quaility Scan") {
          parallel apigeeBuilder.generateSteps("Code Quaility Scan", builddata) {
            switch (it.buildtype) {
              
               
                case "apigee":
                  return apigeeBuilder.code_quality(it)
            }
          }
        }

        // Build
        stage ("Build") {
          parallel apigeeBuilder.generateSteps("Build", builddata) {
            switch (it.buildtype) {
              
              
                case "apigee":
                  return apigeeBuilder.build(it)

            }
          }
        }

        // Unit Tests
        stage ("Unit Tests") {
          parallel apigeeBuilder.generateSteps("Unit Tests", builddata) {
            switch (it.buildtype) {
              
             
                case "apigee":
                  return apigeeBuilder.unittests(it)
            }
          }
        }
// Post build tasks
      if ("${branchName}" =~ /^(?!feature)/) {
        stage ("Post build tasks") {
          parallel apigeeBuilder.generateSteps("Postbuild", builddata) {
            switch (it.buildtype) {
              
            
              case "apigee":
                return apigeeBuilder.postbuild(it)
            }
          }
        }
    }
     // Publish Artifacts
      if ("${branchName}" =~ /^(?!feature)/) {
        stage ("Publish Artifact") {
          parallel apigeeBuilder.generateSteps("Publish Artifacts", builddata) {
            switch (it.buildtype) {
              
              case "apigee":
                return apigeeBuilder.publish(it)
            }
          }
        }
    }

    // Create Artifact Publish Info
  /*  if ("${branchName}" =~ /^(?!feature)/) {
      stage ("Create Artifact Publish Info") {
        dir("${workspace}") {

          def uploadSpec = """{
               "files": [
                 {
                   "pattern": "${repoName}.zip",
                   "target": "libs-snapshot-local/${repoName}/${branchName}/${buildVersion}-${tmpBuilddata["git_shorthash"]}/"
                 }
               ]
             }"""
             def buildInfo = Artifactory.newBuildInfo()
             //artifactoryServer.upload(spec: uploadSpec, buildInfo: buildInfo)
             //artifactoryServer.publishBuildInfo buildInfo
        }
      }
    } */



      } catch (e) {
        if (e instanceof org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException) {
          throw e
        }
        currentBuild.result = 'FAILURE'
        println "ERROR Detected:"
        println e.getMessage()
        def sw = new StringWriter()
        def pw = new PrintWriter(sw)
        e.printStackTrace(pw)
        println sw.toString()
      } finally {


      }
    } // node (k8slabel)


}

return this
