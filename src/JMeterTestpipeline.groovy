
def jmetertest_run(path) {
   if (fileExists('test.yaml'))
     {
     def jmeterTestData = readYaml file: "test.yaml"
     if (jmeterTestData.jmeter != null) {
       jmeterTestData.jmeter.each {
         def templateName = it.name
         def templatePath = it.path
         
           dir("$workspace/${path}/") {
             if ( fileExists("${workspace}/${path}/${templatePath}/${templateName}.jmx") ) {
                 sh """
                   echo $pwd
                   jmeter -Joutput_format=csv -Jresponse_data=true -n -t "${templatePath}/"${templateName}".jmx" -l ${templateName}_Results.csv -j jMeterlog.txt
                   sleep 10
                   jmeter -g heart_beat_Results.csv -o HTMLReports
                   ls
                 """
                 step([$class: 'ArtifactArchiver', artifacts: '**/*.csv'])
                 perfReport (
                   sourceDataFiles:"${templateName}_Results.csv",
                   /*Below field values are configurable based on requrement*/
                   errorFailedThreshold : 6,
                   errorUnstableResponseTimeThreshold : '300',
                   errorUnstableThreshold: 5
                 )

                 def exists = fileExists 'HTMLReports/index.html'
                 if (exists) {
                    echo 'html report file is generated for postman tests'
                    publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'HTMLReports',
                    reportFiles: 'index.html',
                    reportName: "Jmeter Summary Report" ])
                  }
               }
             else {
               println("Jmeter file not found --> ${templateName} did not execute")
             }
           }
         
       }
     }
     else {
       println "Testdata not found --> Jmeter did not execute."
     }
   }
}

return this