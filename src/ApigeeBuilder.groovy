import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret
def generateSteps(job, builddata, cl) {
    return builddata.collectEntries {
        ["${it.name} ${it.version}" : cl(it)]
    }
}
def prebuild(Object buildconfig) {
    return {
        buildconfig.proj.beforeBuild(buildconfig)
    } // end return
}
def code_quality(Object buildconfig) {
        return {
       
            dir("${buildconfig.workingdir}") {
                sh """
                    echo "Nothing To Do"
                """
            }
        
    } // end return
}
def unittests(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "beforeTest", Object)) {
            buildconfig.proj.beforeTest(buildconfig)
        }
       
            dir("${buildconfig.workingdir}") {
                sh """
                    echo "Nothing To Do"
                """
            }
        
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterTest", Object)) {
            buildconfig.proj.afterTest(buildconfig)
        }
    } // end return
}
def build(Object buildconfig) {
    return {
        StandardUsernamePasswordCredentials artifactoryCreds = CredentialsProvider.findCredentialById("NEXUSID",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
        def artifactoryUser = artifactoryCreds.getUsername() 
        def artifactoryPass = Secret.toString(artifactoryCreds.getPassword())
        
           dir("${buildconfig.workingdir}") {
              //  writeFile file: 'apigee-maven-settings.xml', text: libraryResource('apigee-maven-settings.xml')
                sh """
                  cd "${buildconfig.workingdir}/${buildconfig.pomDir}"
                  '${buildconfig.mvnName}/bin/mvn' -DskipTests clean package
                  
                """
            }
        
    } // end return
}

def postbuild(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterBuild", Object)) {
            buildconfig.proj.afterBuild(buildconfig)
        }
    } // end return
}

def publish(Object buildconfig) {
   return {
       if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "beforePush", Object)) {
           buildconfig.proj.beforePush(buildconfig)
       }
     
               withCredentials([[

                                $class          : 'UsernamePasswordMultiBinding',
                                credentialsId   : "NEXUSID",
                                usernameVariable: 'ARTIFACTORY_USERID',
                                passwordVariable: 'ARTIFACTORY_PASSWORD'
                            ]]) {

               dir("${buildconfig.workingdir}/${buildconfig.pomDir}") {
                   sh """
                       cd target/apiproxy
                       mv *.zip ${buildconfig.repoName}.zip
                       cd ..
                   """

                   sh """
				   curl -v -u ${ARTIFACTORY_USERID}:${ARTIFACTORY_PASSWORD}  --upload-file target/apiproxy/${buildconfig.repoName}.zip http://${buildconfig.arttifactserver}/repository/testmaven/${buildconfig.repoName}/${buildconfig.branch}/${buildconfig.version}/${buildconfig.repoName}.zip
				  
                       """
                   buildconfig["build_artifact"] = "http://${buildconfig.arttifactserver}repository/testmaven/${buildconfig.repoName}/${buildconfig.branch}/${buildconfig.version}/${buildconfig.repoName}.zip"
               }
           }    
       
       if (buildconfig.proj.metaClass.respondsTo(buildconfig.proj, "afterPush", Object)) {
           buildconfig.proj.afterPush(buildconfig)
       }
   } // end return
}

return this