import java.io.ByteArrayOutputStream
import hudson.util.StreamTaskListener
import hudson.model.AbstractBuild
import hudson.tasks.Mailer
import hudson.model.User
import org.xml.sax.InputSource
import java.io.StringReader
import hudson.plugins.emailext.ExtendedEmailPublisher
import hudson.plugins.emailext.ExtendedEmailPublisherDescriptor
import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret
import org.apache.commons.jelly.JellyContext
import org.apache.commons.jelly.JellyException
import org.apache.commons.jelly.JellyTagException
import org.apache.commons.jelly.Script
import org.apache.commons.jelly.XMLOutput
import jenkins.model.Jenkins
// Get the email address of the person who last committed
def getGitCommitter() {
    def email = ""
    try {
        email = sh (
            script: 'git --no-pager show -s --format=\'%ae\'',
            returnStdout: true
        ).trim()
    } catch (e) {}
    if (!email) { return "" }
    return email
}
// Get the email address of the person who last ran the job
def getJobSubmitter() {
    def email = ""
    try {
        def build = manager.build
        def cause = build.getCause(hudson.model.Cause.UserIdCause.class)

        if (cause != null) {
            def id = cause.getUserId()
            User user = User.get(id)
            email = user.getProperty(Mailer.UserProperty.class).getAddress();
        }
        if (!email) { 
            // Find if job was triggered by an upstream and get that user email
            def upstreamCause = build.getCause(hudson.model.Cause.UpstreamCause.class)
            if (upstreamCause != null) {
                println "Build Triggered from an Upstream Job"
                def upstreamProject = Jenkins.getInstance().getItemByFullName(upstreamCause.getUpstreamProject(), hudson.model.Job.class)
                if (upstreamProject != null) {
                    println "Located Upstream Project - Now finding specific Build"
                    def upstreamBuild = upstreamProject.getBuildByNumber(upstreamCause.getUpstreamBuild())
                    if (upstreamBuild != null) {
                        println "Found the specific Upstream Build"
                        def upstreamUserCause = upstreamBuild.getCause(hudson.model.Cause.UserIdCause.class)
                        if (upstreamUserCause != null) {
                            def upstreamId = upstreamUserCause.getUserId()
                            if (upstreamId != null) {
                                println "Found Upstream User"
                                User upstreamUser = User.get(upstreamId)
                                email = upstreamUser.getProperty(Mailer.UserProperty.class).getAddress();
                                println "Found User email of Upstream Job : ${email}"
                            }
                        }
                    }
                }
            }
        } 
    } catch (e) {
        println e
    }
    if (!email) { return "" }   
    return email
}
// Send an email using the default plugin
def sendEmail(String emailto, String subject, String body,String mailContent,Boolean attachBuildLog) {
    // Set the SMTP settings
    ExtendedEmailPublisherDescriptor descriptor = Jenkins.getActiveInstance().getDescriptorByType(ExtendedEmailPublisherDescriptor.class)
    
    descriptor.setSmtpServer("smtp.gmail.com")
    descriptor.setSmtpPort("465")
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("SMTP",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    descriptor.setSmtpUsername(creds.getUsername())
    descriptor.setSmtpPassword(Secret.toString(creds.getPassword()))
    // In case we need some advanced settings
    //descriptor.setAdvProperties("mail.smtp.ssl.trust=test0.com")
    descriptor.setUseSsl(true)
	//descriptor.setUseTls(true)
    descriptor.setCharset("UTF-8")
    // Free the memory, causes fault if absent
    descriptor = null
    emailext (
        to: 'moulima.das@zebra.com',
        mimeType: 'text/html',
        subject: subject,
        body: body + mailContent,
        attachLog: attachBuildLog,
        attachmentsPattern: "**/email_attachments/*.*",
        compressLog: true
    )
}
// Send an email to the email address of the person who last committed
def sendToGitCommitter(String subject, String body,String mailContent,Boolean attachBuildLog) {
    def committer = getGitCommitter()
    if (!committer) {
        println "Email not found in git commit, not sending."
        return false
    }
    if (committer.endsWith('@gmail.com') ||
        committer.endsWith('@yahoo.com') ||
        committer.endsWith('@capgemini.com') ||
		committer.endsWith('@zebra.com')	||
        committer.endsWith('@eced.svnit.ac.in')
    ) {
        return sendEmail(committer, subject, body, mailContent,attachBuildLog)
    } else {
        println "Email ${committer} not found in whitelist."
        return false
    }
}
// Send an email to the email address of the person ran the job
def sendToJobSubmitter(String subject, String body,String mailContent,Boolean attachBuildLog) {
    def committer = getJobSubmitter()
    // if (!committer) {
    //     println "Email not found in jenkins job, not sending."
    //     return false
    // }
    // if (committer.endsWith('@gmail.com') ||
    //     committer.endsWith('@yahoo.com') ||
    //     committer.endsWith('@capgemini.com') ||
    //     committer.endsWith('@eced.svnit.ac.in')
    // ) {
    //     return sendEmail(committer, subject, body,mailContent,attachBuildLog)
    // } else {
    //     println "Email ${committer} not found in whitelist."
    //     return false
    // }
    return sendEmail(committer, subject, body,mailContent,attachBuildLog)
}
// Load jelly file from shared library
def loadTemplate(String filename) {
    def tplfile = libraryResource "${filename}"
    def build = currentBuild.rawBuild
    ByteArrayOutputStream stream = new ByteArrayOutputStream()
    StreamTaskListener listener = new StreamTaskListener(stream)
    JellyContext context = new JellyContext()
    ExtendedEmailPublisherDescriptor descriptor = Jenkins.getActiveInstance().getDescriptorByType(ExtendedEmailPublisherDescriptor.class)
    context.setVariable("it", new Object())
    context.setVariable("build", build)
    context.setVariable("project", build.getParent())
    context.setVariable("logger", listener.getLogger())
    context.setVariable("rooturl", descriptor.getHudsonUrl())
    Script script = context.compileScript(new InputSource(new StringReader(tplfile)))
    if (script != null) {
        ByteArrayOutputStream output = new ByteArrayOutputStream(16 * 1024)
        XMLOutput xmlOutput = XMLOutput.createXMLOutput(output)
        script.run(context, xmlOutput)
        xmlOutput.flush()
        xmlOutput.close()
        output.close()
        return output.toString(ExtendedEmailPublisher.descriptor().getCharset())
    }
    return null
}
// Send build email to the email address of the person who last committed
def sendBuildToGitCommitter(String mailContent) {
    def result = currentBuild.currentResult.toString()
    def attachBuildLog = (result == 'FAILURE') ? true : false
    try {
        sendToGitCommitter("Build Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' ${result}", loadTemplate('jenkins-email-template.jelly'),mailContent,attachBuildLog)
    } catch (e) {
        println "Sending Email failed"
    }
}
// Send deployment email to the email address of the person running the job
def sendDeployToJobSubmitter(String mailContent) {
    def result = currentBuild.currentResult.toString()
    def attachBuildLog = (result == 'FAILURE') ? true : false
    try {
        sendToJobSubmitter("Deploy Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' ${result}", loadTemplate('jenkins-email-template.jelly'),mailContent,attachBuildLog)
    } catch (e) {
        println "Sending Email failed"
        println e
    }
}


return this