
import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret

import groovy.json.JsonSlurper


// Sorting requires NonCPS
@NonCPS
def getDeploymentArtifactUrl(reponame,artifactory_url) {
    def buildToDeploy = params["BuildToDeploy"].toLowerCase()
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("NEXUSID",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def artifactoryUser = creds.getUsername()
    def artifactoryPass = Secret.toString(creds.getPassword())
    def apiUrl = "http://${artifactory_url}/artifactory/api/search/aql".toURL().openConnection()
    def basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary("${artifactoryUser}:${artifactoryPass}".getBytes())
    apiUrl.setRequestMethod("POST")
    apiUrl.setRequestProperty("Authorization", basicAuth)
    apiUrl.setRequestProperty("content-type", "text/plain; charset=utf-8")
    apiUrl.setDoOutput(true)
    def writer = new OutputStreamWriter(apiUrl.outputStream)
    def query
    if (buildToDeploy == 'latest') {
      query = 'items.find({"repo":{"$eq":"devops-deployments"},"path":{"$match": "' +  reponame + '/*"}})'
    } else {
      buildToDeploy = buildToDeploy.split(" -- ").last()
      query = 'items.find({"repo":{"$eq":"devops-deployments"},"path":{"$match": "' + reponame + '/*/' + buildToDeploy + '"}})'
    }
    println query
    writer.write(query)
    writer.close()
    apiUrl.connect()
    def json = new JsonSlurper().parse(apiUrl.getInputStream())
    ArrayList jsonResults = json.results
    ArrayList sortedJson = jsonResults.sort { a, b ->
      return b.created <=> a.created
    }
    println sortedJson
    def topBuild = sortedJson.first()
    return "https://artifactrepository.mcd.com/artifactory/vet-deployments/${topBuild.path}/${topBuild.name}"
}


def generateSteps(job, builddata, cl) {
    return builddata.components.collectEntries {
        ["${it.name} ${it.version}" : cl(it)]
    }
}

def getDeploymentArtifactUrl(String reponame) {
    
}

def terraform_scan(String terraformPath) {
    
        dir("${terraformPath}") {
            sh """
                
                ls -al
            """
        }
}

def pre_deploy(Object buildconfig) {
    return {
        if (buildconfig.proj.metaClass.respondsTo(buildconfig.mcdProj, "beforeDeploy", Object)) {
            buildconfig.proj.beforeDeploy(buildconfig)
        }
    } // end return
}

//TODO: Need to specify *standard* location of zip files etc. used by terraform so we can download using deployment.yaml
def terraform_apply(applyChanges, terraformPath, configFile, jobName, gitShortCode, buildNames, buildArtifacts, buildVersions,scmBrowseBranch) {


    println """
        export TF_VAR_jobname=${jobName}
        export TF_VAR_gitshortcode=${gitShortCode}
        export TF_VAR_buildnames=${buildNames}
        export TF_VAR_buildartifacts=${buildArtifacts}
        export TF_VAR_buildversions=${scmBrowseBranch}-${buildVersions}_${gitShortCode}
        export TF_VAR_artifact_type=${scmBrowseBranch}
    """
    
    def commonScript = """
        set -e

        export TF_VAR_jobname=${jobName}
        export TF_VAR_gitshortcode=${gitShortCode}
        export TF_VAR_buildnames=${buildNames}
        export TF_VAR_buildartifacts=${buildArtifacts}
        export TF_VAR_buildversions=${scmBrowseBranch}-${buildVersions}_${gitShortCode}
        export TF_VAR_artifact_type=${scmBrowseBranch}

        export DATAFILE=configurations/${configFile}
        export ENVIRONMENT=\$(sed -nr 's/^\\s*environment\\s*=\\s*"([^"]*)".*\$/\\1/p' \$DATAFILE)
       

        terraform init
""";
    if (applyChanges) {
       
            dir("${terraformPath}") {
                println "Applying"
                writeFile file: 'Devops-key-pair-cg-account.pem', text: libraryResource('Devops-key-pair-cg-account.pem')
				withCredentials([[
                    $class: 'UsernamePasswordMultiBinding',
                    credentialsId: "GIT_ACCESS",
                    usernameVariable: 'GIT_ACCESS_USERID',
                    passwordVariable: 'GIT_ACCESS_PASSWORD'
                ]]) {
					wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
						sh """git config --global credential.helper '!f() { sleep 1; echo "username=${GIT_ACCESS_USERID}\npassword=${GIT_ACCESS_PASSWORD}"; }; f'
						${commonScript}
						echo | terraform plan --var-file=\$DATAFILE
						echo | terraform apply --auto-approve --var-file=\$DATAFILE
						"""
					}
				}
            
        }
    } else {
       
            dir("${terraformPath}") {
                println "Planning"
				withCredentials([[
                    $class: 'UsernamePasswordMultiBinding',
                    credentialsId: "GIT_ACCESS",
                    usernameVariable: 'GIT_ACCESS_USERID',
                    passwordVariable: 'GIT_ACCESS_PASSWORD'
                ]]) {
					wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
						sh """git config --global credential.helper '!f() { sleep 1; echo "username=${GIT_ACCESS_USERID}\npassword=${GIT_ACCESS_PASSWORD}"; }; f'
						${commonScript}
						echo | terraform plan --var-file=\$DATAFILE
						"""
					}
				}
            }
        
    }
}

def post_deploy(Object buildconfig) {
    return {
        if (buildconfig.mcdProj.metaClass.respondsTo(buildconfig.mcdProj, "afterDeploy", Object)) {
            buildconfig.mcdProj.afterDeploy(buildconfig)
        }
    } // end return
}
def deployment_artifact_pull(jobname) {
    // StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("NEXUSID",
    //     StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    // def artifactoryUser = creds.getUsername()
    // def artifactoryPass = Secret.toString(creds.getPassword())

    
        dir("${workspace}") {
            // def url = getDeploymentArtifactUrl(jobname)
            // sh """
            //     curl --fail -u ${artifactoryUser}:${artifactoryPass} -o deployment.zip ${url}
            //     unzip deployment.zip
            //     cat deployment.yaml
            // """

        }
    

}

def docker_artifact_pull(Object buildconfig, Object deploymentdata) {
    return {
    } // end return
}



def muleonprem_artifact_pull(String git_commit_short_head, String branch,Object deploymentdata) {
  
       StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("NEXUSID",
       StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
       def artifactoryUser = creds.getUsername()
       def artifactoryPass = Secret.toString(creds.getPassword())
      
            dir("${workspace}/${deploymentdata.deployment.terraform_path}") {
               sh """
                    echo "Downloading the artifact to deploy"
                    wget http://localhost:1234/repository/testmaven/${deploymentdata.apigee[0].name}/${branch}/${deploymentdata.version}_${git_commit_short_head}/${deploymentdata.apigee[0].name}.zip
               """
           }
       

}


def getSingleFileFromGit(String httpsUrl, String branch) {
    StandardUsernamePasswordCredentials creds = CredentialsProvider.findCredentialById("zebra",
            StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
    def gitUser = creds.getUsername()
    def gitPass = Secret.toString(creds.getPassword())
    String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary("${gitUser}:${gitPass}".getBytes())
    def configUrl = "${httpsUrl}?at=${branch}&raw"
    URLConnection connection
    InputStream inputStream
    //def err

    connection = configUrl.toURL().openConnection()
    connection.setRequestProperty("Authorization", basicAuth)
    inputStream = connection.getInputStream()
/*
    try {
      inputStream = connection.getInputStream()
    } catch (e) {
      inputStream = connection.getErrorStream()
      err = e.toString()
    }
*/ 
    return inputStream.getText()
}


return this
