
def postmantest_run(path) {
   if (fileExists('test.yaml'))
     {
     def postmanTestData = readYaml file: "test.yaml"
     if (postmanTestData.postman != null) {
       postmanTestData.postman.each {
         def templateName = it.name
         def templatePath = it.path
         def templateStyle = it.style
         def templateEnv = it.env
         
           dir("$workspace/${path}/") {
             if ( fileExists("${workspace}/${path}/${templatePath}/${templateName}.json") ) {
                 sh """
                   echo $pwd
                   newman run "${templatePath}/"${templateName}".json" -e ${templateEnv} -r html,cli --reporter-html-export reports/result.html --reporter-html-template ${templateStyle}
                   sleep 10
                 """
                 step([$class: 'ArtifactArchiver', artifacts: 'reports/result.html'])
                 def exists = fileExists 'reports/result.html'
                 if (exists) {
                    echo 'html report file is generated for postman tests'
                    publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: 'reports',
                    reportFiles: 'result.html',
                    reportName: "Postman Report" ])
                  }
               }
             else {
               println("Postman script file not found --> ${templateName} did not execute")
             }
           }
         
       }
     }
     else {
       println "Testdata not found --> Postman did not execute."
     }
   }
}

return this